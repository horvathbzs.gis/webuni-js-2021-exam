import { loadData } from "./covid-cases";
import { displayData } from "./covid-cases";


export const initForm = () => {
    const form = document.getElementById('form');
    const errorMessage = document.getElementById('error-message');
    const errorMessage2 = document.getElementById('error-message2');
    const submitButton = document.getElementById('submit');
    const dataContainer = document.getElementById('data-container');
    

    form.addEventListener('submit', async e => {
        dataContainer.innerHTML = '';
        const country = document.getElementById('country-input').value;
        console.log('form.js > initForm() > country: ' + country);
        e.preventDefault();
        let firstLetter = country[0];
        if (firstLetter !== firstLetter.toUpperCase()) {
            errorMessage2.style.display = 'block'
            setTimeout(() => errorMessage2.style.display = 'none', 2000);
        } else {
            submitButton.disabled = true;
            dataContainer.insertAdjacentHTML('afterbegin', `<div id="loading-indicator" class="loader"></div>`);
            try {
                const countryData = await loadData(country);
                displayData(countryData);
                form.reset();
            } catch {
                errorMessage.style.display = 'block';
                setTimeout(() => errorMessage.style.display = 'none', 2000);
            }
            submitButton.disabled = false;
            dataContainer.removeChild(document.getElementById('loading-indicator'));
        }
    });
}