import { setMap } from "./osm-map";

/**
 * Export method calls getCases() and getVaccines methods and handles errors.
 * @param {*} country
 * @returns countryData combined data of the two method return values.
 */
export const loadData = async (country) => {
    try {
        const countryCases = await getCases(country);
        //console.log('covid-cases.js > loadData() > countryCases: ');
        //console.log(countryCases);

        const countryVaccines = await getVaccines(country);
        //console.log('covid-cases.js > loadData() > countryVaccines: ');
        //console.log(countryVaccines);

        const countryData = {
            country: countryCases.country,
            abbreviation: countryCases.abbreviation,
            population: countryCases.population,
            confirmed: countryCases.confirmed,
            deaths: countryCases.deaths,
            vaccinated: countryVaccines.people_vaccinated,
            lat: countryCases.lat,
            long: countryCases.long
        };

        console.log('covid-cases.js > loadData() > countryData: ');
        console.log(countryData);

        return countryData;
    } catch (e) {
        console.error('covid-cases.js > loadData() > Error loading country covid data ', e);
        throw e;
    }
}

/**
 * Creates html tags to display country covid cases and vaccines data.
 * Adds OSM map to map div. Removes ol map if previously existed on page.
 */
export const displayData = async (countryData) => {
    const container = document.getElementById('data-container');
    const map = document.getElementById('map');
    map.style.display = 'none';

    const population = thousandSeparator(countryData.population);
    const confirmed = thousandSeparator(countryData.confirmed);
    const deaths = thousandSeparator(countryData.deaths);
    const vaccinated = thousandSeparator(countryData.vaccinated);
    const lat = countryData.lat;
    const long = countryData.long;
    console.log('covid-cases.js > displayData() > lat: ' + lat);
    console.log('covid-cases.js > displayData() > long: ' + long);

    // --- add OSM map ---
    if (countryData.long != null && countryData.lat != null) {
        map.style.display = 'block';
        if (map.hasChildNodes()) {
            map.removeChild(map.childNodes[0]);
          } 
        await setMap(long, lat);
    }

    //container.removeChild(document.getElementById('countryDataTable'));
    container.innerHTML = '';
    container.innerHTML = container.innerHTML + `
        <div id=countryDataTable class="divTable cinereousTable">
            <div class="divTableHeading">
                <div class="divTableRow">
                    <div class="divTableHead">${countryData.country}</div>
                    <div class="divTableHead">${countryData.abbreviation}</div>
                </div>
            </div>
            <div class="divTableBody">
                <div class="divTableRow">
                    <div class="divTableCell">Teljes lakosság</div><div class="divTableCell">${population}</div>
                </div>
                <div class="divTableRow">
                    <div class="divTableCell">Regisztrált eset</div><div class="divTableCell">${confirmed}</div>
                </div>
                <div class="divTableRow">
                    <div class="divTableCell">Fertőzésben elhunytak</div><div class="divTableCell">${deaths}</div>
                </div>
                <div class="divTableRow">
                    <div class="divTableCell">Oltottak száma</div><div class="divTableCell">${vaccinated}</div>
                </div>               
            </div>
        </div>`;
}

/**
 * Separate number by thousands.
 * @param {*} num 
 * @returns thousond separated number 
 */
const thousandSeparator = (num) => {
    let num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return num_parts.join(",");
}

/**
 * Calls covid-api cases endpoint. In the response JSON, All array contains data for the country.
 * Some country has territorial data. This method returns only with data for the whole country.F 
 * @param {*} country query param of endpoint
 * @returns with JSON response.All
 */
const getCases = async (country) => {
    const response = await fetch(`https://covid-api.mmediagroup.fr/v1/cases/?country=${country}`);
    if (response.status !== 200) {
        throw 'Error loading country cases';
    }
    const jsonResponse = await response.json();
    return jsonResponse.All;
}

/**
 * Calls covid-api cases vaccines. In the response JSON, All array contains data for the country.
 * Some country has territorial data. This method returns only with data for the whole country.F 
 * @param {*} country query param of endpoint
 * @returns with JSON response.All
 */
const getVaccines = async (country) => {
    const response = await fetch(`https://covid-api.mmediagroup.fr/v1/vaccines/?country=${country}`);
    if (response.status !== 200) {
        throw 'Error loading country cases';
    }
    const jsonResponse = await response.json();
    return jsonResponse.All;
}