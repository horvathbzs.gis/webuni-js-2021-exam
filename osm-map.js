import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { transform } from 'ol/proj';

/**
 * Sets a OSM map with center given in params.
 * Transformation from WGS84 to OSM projection.
 * @param {*} long WGS84 longitude
 * @param {*} lat WGS84 lattitude
 */
export const setMap = async (long, lat) => {
  let coord_EPSG3857 = transform([long, lat], 'EPSG:4326', 'EPSG:3857');
  console.log('osm-map.js > setMap() > coord_EPSG3857: ' + coord_EPSG3857);

  const map = new Map({
    target: 'map',
    layers: [
      new TileLayer({
        source: new OSM()
      })
    ],
    view: new View({
      center: coord_EPSG3857,
      zoom: 6
    })
  });
}