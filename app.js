import { initForm } from './form';
import './styles.css';

window.addEventListener('DOMContentLoaded', () => {
    initForm();
});